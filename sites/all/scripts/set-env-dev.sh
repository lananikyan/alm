#! /bin/bash

drush dis -y memcache memcache_admin cdn
drush en -y reroute_email stage_file_proxy

drush vset reroute_email_address sitemgr@demonzmedia.com
drush vset reroute_email_enable 1

drush cc all
