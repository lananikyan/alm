<?php

function update_1_0_0_mobilemenu() {

  // New menu link
  $item = array(
    'menu_name' => 'menu-mobile-main-menu',
    'link_path' => 'store-locator',
    'link_title' => 'Store locator',
    'options' => array(),
    'weight' => -50,
    'module' => 'alm_magic',
  );
  menu_link_save($item);

  $item = array(
    'menu_name' => 'menu-mobile-main-menu',
    'link_path' => 'specials',
    'link_title' => 'Specials',
    'options' => array(),
    'weight' => -45,
    'module' => 'alm_magic',
  );
  menu_link_save($item);


  $item = array(
    'menu_name' => 'menu-mobile-main-menu',
    'link_path' => 'search/site',
    'link_title' => 'Search',
    'options' => array(),
    'weight' => -40,
    'module' => 'alm_magic',
  );
  menu_link_save($item);

}
