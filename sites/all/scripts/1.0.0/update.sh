#!

drush en -y clientside_validation clientside_validation_webform
drush fr -y feat_fields feat_views feat_variables
echo "Replace store_detail_map with store_locator_landing_map on Store Locator landing page"

drush --uri=http://cellarbrations.com.au eval "alm_store_locator_create_redirects();"
drush --uri=http://thebottle-o.com.au eval "alm_store_locator_create_redirects();"
drush --uri=http://liquor.iga.com.au eval "alm_store_locator_create_redirects();"
drush --uri=http://duncans.com.au eval "alm_store_locator_create_redirects();"


drush eval "include './sites/all/scripts/1.0.0/update.php'; update_1_0_0_mobilemenu();"
