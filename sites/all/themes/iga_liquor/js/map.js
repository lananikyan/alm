/*
 * The google.maps.event.addListener() event waits for
 * the creation of the infowindow HTML structure 'domready'
 * and before the opening of the infowindow defined styles
 * are applied.
 */


jQuery(function () {

    var miniSearchForm = jQuery('#block-search-form');
    var searchLink = jQuery('.search-link');
    searchLink.click(function () {
        var els = miniSearchForm.find('.form-item,.form-actions');
        console.log('els ' + els.length);
        var hide = jQuery(this).data('hidden');
        if (hide) {
            els.hide();
            miniSearchForm.removeClass('block-search-visible');
        } else {
            els.show();
            miniSearchForm.addClass('block-search-visible');
        }
        jQuery(this).data('hidden', !hide);
        return false;
    });

    //setTimeout(function () {
    //    // Reference to the DIV which receives the contents of the infowindow using jQuery
    //    //var iwOuter = jQuery('.gm-style-iw');       
    //    //iwOuter.css("border", "10px solid red");
    //    ///* The DIV we want to change is above the .gm-style-iw DIV.
    //    // * So, we use jQuery and create a iwBackground variable,
    //    // * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
    //    // */
    //    //var iwBackground = iwOuter.prev();
    //    //// Remove the background shadow DIV
    //    //iwBackground.children(':nth-child(2)').css({ 'display': 'none' });
    //    //// Remove the white background DIV
    //    //iwBackground.children(':nth-child(4)').css({ 'display': 'none' });
    //    //// Moves the infowindow 115px to the right.
    //    //iwOuter.parent().parent().css({ left: '115px' });
    //    //// Moves the shadow of the arrow 76px to the left margin 
    //    //iwBackground.children(':nth-child(1)').attr('style', function (i, s) { return s + 'left: 76px !important;' });
    //    //// Moves the arrow 76px to the left margin 
    //    //iwBackground.children(':nth-child(3)').attr('style', function (i, s) { return s + 'left: 76px !important;' });
    //    //// Changes the desired color for the tail outline.
    //    //// The outline of the tail is composed of two descendants of div which contains the tail.
    //    //// The .find('div').children() method refers to all the div which are direct descendants of the previous div. 
    //    //iwBackground.children(':nth-child(3)').find('div').children().css({ 'box-shadow': 'rgba(72, 181, 233, 0.6) 0px 1px 6px', 'z-index': '1' });
    //    //var iwCloseBtn = iwOuter.next();
    //    //iwCloseBtn.css({ 'display': 'none' });
    //}, 3000);

});