<?php

// deactivated in favour of individual templates
function cellarbrations_preprocess_alm_sutro(&$variables) {
  // iterate through all panes content and extract some theming information to be
  // attach to panel wrapping markup for easier styling:
  // - field_colour, field_layout -> as css class
  // -field_module_image -> as background-image
//  dpm($variables['content'], '$variables[content]');
//  dpm($variables['display'], '$variables[display]');

  foreach (array_keys($variables['content']) as $region_name) {
    if (isset($variables['display']->panels[$region_name])) {
      // we can have several content in the same region:
      // for css classes: we will compute and add properties related to all of them
      // for background image: we will keep just the last one:
      $extra_classes = array();
      $background_image = '';
      foreach ($variables['display']->panels[$region_name] as $pid) { // iterate to all panel_content_id (pid)

//        dpm($variables['display']->content[$pid], '$variables[display]->content[$pid]');

        // Add class for component type/subtype
        $extra_classes[] = $variables['display']->content[$pid]->type . '-' . $variables['display']->content[$pid]->subtype;

        if ($variables['display']->content[$pid]->type == 'node') {
          $nid = $variables['display']->content[$pid]->configuration['nid'];
          $query = db_select('node', 'n');
          $query->addField('n', 'type');
          $query->condition('nid', $nid);
          $type = $query->execute()->fetchField();
//          dpm($type, 'type');
          if ($type) {
            $extra_classes[] = 'node-' . $type;
          }
        }


//        if (isset($variables['display']->content[$pid]->type) && $variables['display']->content[$pid]->type == 'node'
//        && isset($variables['display']->content[$pid]->configuration['nid'])) {
//          $node = node_load($variables['display']->content[$pid]->configuration['nid']);
//          if (isset($node->field_colour[LANGUAGE_NONE][0]['value'])) {
//            $extra_classes[] = 'colour-' . drupal_html_class($node->field_colour[LANGUAGE_NONE][0]['value']);
//          }
//          if (isset($node->field_layout[LANGUAGE_NONE][0]['value'])) {
//            $extra_classes[] = 'layout-' . drupal_html_class($node->field_layout[LANGUAGE_NONE][0]['value']);
//          }
//          if (isset($node->field_module_image[LANGUAGE_NONE][0]['uri'])) {
////            $background_image = image_style_url('landscape_16_9', $node->field_module_image[LANGUAGE_NONE][0]['uri']);
//          }
//        }
      } // Iteration through panel_content - END
      // add information to the variables (to be used in template alm-sutro.tpl.php
      $variables['region_classes'][$region_name] = implode(' ', $extra_classes);

//      if (!empty($background_image)) {
//        $variables['region_background_image'][$region_name] = $background_image;
//      }
    }
  }
//  dpm($variables['region_classes'], '$variables[region_classes]');
//  dpm($variables, '$variables');
}

function cellarbrations_preprocess_panels_pane(&$variables) {
//  dpm($variables, '$variables');

  // Find out the node type and add it to the panel pane wrapper class
  if ($variables['pane']->type == 'node') {
    $nid = $variables['pane']->configuration['nid'];
    $query = db_select('node', 'n');
    $query->addField('n', 'type');
    $query->condition('nid', $nid);
    $type = $query->execute()->fetchField();

    if ($type) {
      $variables['classes_array'][] = 'pane-' . $type;
    }
  }
}


/**
 * Override or insert variables into the page template.
 */
function cellarbrations_process_page(&$variables) {
  // Hook into color.module.
  if (module_exists('color')) {
    _color_page_alter($variables);
  }
  // Always print the site name and slogan, but if they are toggled off, we'll
  // just hide them visually.
  $variables['hide_site_name'] = theme_get_setting('toggle_name') ? FALSE : TRUE;
  $variables['hide_site_slogan'] = theme_get_setting('toggle_slogan') ? FALSE : TRUE;
  if ($variables['hide_site_name']) {
    // If toggle_name is FALSE, the site_name will be empty, so we rebuild it.
    $variables['site_name'] = filter_xss_admin(variable_get('site_name', 'Drupal'));
  }
  if ($variables['hide_site_slogan']) {
    // If toggle_site_slogan is FALSE, the site_slogan will be empty, so we rebuild it.
    $variables['site_slogan'] = filter_xss_admin(variable_get('site_slogan', ''));
  }
  // Since the title and the shortcut link are both block level elements,
  // positioning them next to each other is much simpler with a wrapper div.
  if (!empty($variables['title_suffix']['add_or_remove_shortcut']) && $variables['title']) {
    // Add a wrapper div using the title_prefix and title_suffix render elements.
    $variables['title_prefix']['shortcut_wrapper'] = array(
      '#markup' => '<div class="shortcut-wrapper clearfix">',
      '#weight' => 100,
    );
    $variables['title_suffix']['shortcut_wrapper'] = array(
      '#markup' => '</div>',
      '#weight' => -99,
    );
    // Make sure the shortcut link is the first item in title_suffix.
    $variables['title_suffix']['add_or_remove_shortcut']['#weight'] = -100;
  }

  $variables['mobile_main_menu'] = menu_navigation_links('menu-mobile-main-menu');

//  dpm($variables, '$variables');
}


function cellarbrations_field($variables) {
  if ($variables['element']['#bundle'] == 'store') {
    // remove the colon after the label for stores
    $label_suffix = '&nbsp;';
  }
  else {
    $label_suffix = ':&nbsp;';
  }

  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . $label_suffix . '</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function cellarbrations_field__link_field($variables) {
  if ($variables['element']['#bundle'] == 'store') {
    // remove the colon after the label for stores
    $label_suffix = '&nbsp;';
  }
  else {
    $label_suffix = ':&nbsp;';
  }

  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<div class="field-label"' . $variables['title_attributes'] . '>' . $variables['label'] . $label_suffix . '</div>';
  }

  // Render the items.
  $output .= '<div class="field-items"' . $variables['content_attributes'] . '>';
  foreach ($variables['items'] as $delta => $item) {
    // add a class to each link element
    $class = 'link-' . str_replace(array(' '), '', strtolower($item['#element']['title']));
    $item['#element']['attributes']['class'] = $class;

    // open external links in a new window
    global $base_url;
    if (strpos($item['#element']['display_url'], $base_url) === FALSE) {
      $item['#element']['attributes']['rel'] = 'nofollow';
      $item['#element']['attributes']['target'] = '_blank';
//      $item['#element']['title'] = $item['#element']['title'] . ' <span class="element-invisible link-external">External link, opens in new window</span>';
//      $item['#element']['html'] = TRUE;
    }
    else {
      // remove nofollow attribute from internal links
      unset($item['#element']['attributes']['rel']);
    }

    $classes = 'field-item ' . ($delta % 2 ? 'odd' : 'even');
    $output .= '<div class="' . $classes . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</div>';
  }
  $output .= '</div>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

/**
 * Implements theme_field__field_type().
 */
function cellarbrations_field__taxonomy_term_reference($variables) {
  if ($variables['element']['#bundle'] == 'store') {
    // remove the colon after the label for stores
    $label_suffix = '&nbsp;';
  }
  else {
    $label_suffix = ':&nbsp;';
  }

  $output = '';

  // Render the label, if it's not hidden.
  if (!$variables['label_hidden']) {
    $output .= '<h3 class="field-label">' . $variables['label'] . $label_suffix . '</h3>';
  }

  // Render the items.
  $output .= ($variables['element']['#label_display'] == 'inline') ? '<ul class="links inline">' : '<ul class="links">';
  foreach ($variables['items'] as $delta => $item) {
    $output .= '<li class="taxonomy-term-reference-' . $delta . '"' . $variables['item_attributes'][$delta] . '>' . drupal_render($item) . '</li>';
  }
  $output .= '</ul>';

  // Render the top-level DIV.
  $output = '<div class="' . $variables['classes'] . (!in_array('clearfix', $variables['classes_array']) ? ' clearfix' : '') . '"' . $variables['attributes'] . '>' . $output . '</div>';

  return $output;
}

function cellarbrations_links__system_main_menu($variables) {
//  dpm($variables, '$variables');

  $links = $variables['links'];
  $attributes = $variables['attributes'];
  $heading = $variables['heading'];
  global $language_url;
  $output = '';

  if (count($links) > 0) {
    // Treat the heading first if it is present to prepend it to the
    // list of links.
    if (!empty($heading)) {
      if (is_string($heading)) {
        // Prepare the array that will be used when the passed heading
        // is a string.
        $heading = array(
          'text' => $heading,
          // Set the default level of the heading.
          'level' => 'h2',
        );
      }
      $output .= '<' . $heading['level'];
      if (!empty($heading['class'])) {
        $output .= drupal_attributes(array('class' => $heading['class']));
      }
      $output .= '>' . check_plain($heading['text']) . '</' . $heading['level'] . '>';
    }

    $num_links = count($links);
    $i = 1;

    $attributes['class'][] = "num-links-$num_links";

    $output .= '<ul' . drupal_attributes($attributes) . '>';

    foreach ($links as $key => $link) {
      $class = array($key);
      $class[] = 'menu-' . str_replace('/', '--', $link['href']);

      // Add first, last and active classes to the list of links to help out themers.
      if ($i == 1) {
        $class[] = 'first';
      }
      if ($i == $num_links) {
        $class[] = 'last';
      }
      if (isset($link['href']) && ($link['href'] == $_GET['q'] || ($link['href'] == '<front>' && drupal_is_front_page()))
        && (empty($link['language']) || $link['language']->language == $language_url->language)
      ) {
        $class[] = 'active';
      }
      $output .= '<li' . drupal_attributes(array('class' => $class)) . '>';

      // some extra markup inside the a main menu
      // a blank icon class
      $new_title = '<span class="icon"></span>';

      // and the text wrapped inside a .text class
      $new_title .= '<span class="text">' . $link['title'] . '</span>';

      $link['html'] = TRUE;

      if (isset($link['href'])) {
        // Pass in $link as $options, they share the same keys.
        $output .= l($new_title, $link['href'], $link);
      }
      elseif (!empty($link['title'])) {
        // Some links are actually not links, but we wrap these in <span> for adding title and class attributes.
//        if (empty($link['html'])) {
//          $link['title'] = check_plain($link['title']);
//        }
        $span_attributes = '';
        if (isset($link['attributes'])) {
          $span_attributes = drupal_attributes($link['attributes']);
        }
        $output .= '<span' . $span_attributes . '>' . $new_title . '</span>';
      }

      $i++;
      $output .= "</li>\n";
    }

    $output .= '</ul>';
  }

  return $output;
}

/**
 * Returns HTML for a node preview for display during node creation and editing.
 *
 * @param $variables
 *   An associative array containing:
 *   - node: The node object which is being previewed.
 *
 * @see node_preview()
 * @ingroup themeable
 */
function cellarbrations_node_preview($variables) {
  $node = $variables['node'];

  $output = '<div class="preview-wrapper">';

  $preview_trimmed_version = FALSE;

//  $elements = node_view(clone $node, 'teaser');
//  $trimmed = drupal_render($elements);
  $elements = node_view($node, 'full');
  $full = drupal_render($elements);

//  // Do we need to preview trimmed version of post as well as full version?
//  if ($trimmed != $full) {
//    drupal_set_message(t('The trimmed version of your post shows what your post looks like when promoted to the main page or when exported for syndication.<span class="no-js"> You can insert the delimiter "&lt;!--break--&gt;" (without the quotes) to fine-tune where your post gets split.</span>'));
//    $output .= '<h3>' . t('Preview trimmed version') . '</h3>';
//    $output .= $trimmed;
//    $output .= '<h3>' . t('Preview full version') . '</h3>';
//    $output .= $full;
//  }
//  else {
  $output .= $full;
//  }
  $output .= "</div>\n";

  return $output;
}
