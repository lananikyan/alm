<?php
/**
 * @file
 * Template for Radix Boxton.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
//dpm(get_defined_vars(), 'get_defined_vars');

if (empty($classes)) {
  $classes = '';
}

if (empty($class)) {
  $class = '';
}
else {
  $classes .= " $class";
}

if (empty($css_id)) {
  $css_id = '';
}
else {
  $css_id = "id=\"$css_id\"";
}


if (isset($field_storefeature_icon[0]['uri'])) {
  $inline_image = file_get_contents($field_storefeature_icon[0]['uri']);
}
else {
  $inline_image = '';
}

?>

<div
  class="panel-display boxton clearfix <?php print $classes; ?>" <?php print $css_id; ?>>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 radix-layouts-content panel-panel">
        <div class="panel-panel-inner">
          <div class="icon"><?php print $inline_image; ?></div>
          <?php print $content['contentmain']; ?>
        </div>
      </div>
    </div>
  </div>

</div><!-- /.boxton -->
