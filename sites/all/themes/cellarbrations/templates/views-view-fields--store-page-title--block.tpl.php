<?php

/**
 * @file
 * Default simple view template to all the fields as a row.
 *
 * - $view: The view in use.
 * - $fields: an array of $field objects. Each one contains:
 *   - $field->content: The output of the field.
 *   - $field->raw: The raw data for the field, if it exists. This is NOT output safe.
 *   - $field->class: The safe class id to use.
 *   - $field->handler: The Views field handler object controlling this field. Do not use
 *     var_export to dump this object, as it can't handle the recursion.
 *   - $field->inline: Whether or not the field should be inline.
 *   - $field->inline_html: either div or span based on the above flag.
 *   - $field->wrapper_prefix: A complete wrapper containing the inline_html to use.
 *   - $field->wrapper_suffix: The closing tag for the wrapper.
 *   - $field->separator: an optional separator that may appear before a field.
 *   - $field->label: The wrap label text to use.
 *   - $field->label_html: The full HTML of the label to use including
 *     configured element type.
 * - $row: The raw result object from the query, with all data it fetched.
 *
 * @ingroup views_templates
 */

//dpm($fields, '$fields');

$fields1 = array(
  'title' => $fields['title'],
);
//$fields2 = array(
//  'field_store_trading_hours' => $fields['field_store_trading_hours'],
//  'field_geofield_distance' => $fields['field_geofield_distance'],
//);

$fields2 = module_invoke('alm_store_locator', 'block_view', 'store_details_distance');

// make this element an ajaxblock
$block = array();
$block['block'] = new stdClass();
$block['block']->module = 'alm_store_locator';
$block['block']->delta = 'store_details_distance';
$block['content'] = $fields2['content']['view']['#markup'];
alm_store_locator_preprocess_block($block);
$fields2['content']['view']['#markup'] = $block['content'];
unset($fields2['content']['geoinfo']);

//dpm($block, '$block');
//dpm($fields2, '$fields2');
?>
<?php foreach ($fields1 as $id => $field): ?>
  <?php if (!empty($field->separator)): ?>
    <?php print $field->separator; ?>
  <?php endif; ?>

  <?php print $field->wrapper_prefix; ?>
  <?php print $field->label_html; ?>
  <?php print $field->content; ?>
  <?php print $field->wrapper_suffix; ?>
<?php endforeach; ?>

<div class="views-field-field-store-trading-hours-and-distance">
  <?php print render($fields2); ?>
</div>
