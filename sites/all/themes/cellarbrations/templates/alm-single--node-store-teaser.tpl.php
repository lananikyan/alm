<?php
/**
 * @file
 * Template for Radix Boxton.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */

//dpm(get_defined_vars(), 'get_defined_vars');

if (empty($classes)) {
  $classes = '';
}

if (empty($class)) {
  $class = '';
}
else {
  $classes .= " $class";
}

if (empty($css_id)) {
  $css_id = '';
}
else {
  $css_id = "id=\"$css_id\"";
}

$classes .= ' section-who-we-are';

$classes .= ' image-placement-left';

//$path = drupal_get_path('theme', $GLOBALS['theme']);
//$background_image = url($path . '/who-we-are.jpg', array('absolute' => TRUE));

$domain = domain_get_domain();
$background_image = image_style_url('alm_halfpage_684x495', "public://who_we_are_images/{$domain['machine_name']}.jpeg");
$background_style = "style=\"background-image: url('$background_image');\"";

?>

<div
  class="panel-display half-module col-md-12 clearfix <?php print $classes; ?>" <?php print $css_id; ?> <?php print $background_style; ?>>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 radix-layouts-content panel-panel">
        <div class="panel-panel-inner">
          <div
            class="field field-name-field-module-image field-type-image field-label-hidden">
            <div class="field-items">
              <div class="field-item even">
                <img width="684" height="495" alt="" src="<?php print $background_image ?>" typeof="foaf:Image" class="alm-halfpage-684x495">
              </div>
            </div>
          </div>
          <?php print $content['contentmain']; ?>
        </div>
      </div>
    </div>
  </div>

</div><!-- /.boxton -->
