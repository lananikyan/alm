<?php
/**
 * @file
 * Template for Radix Boxton.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */

//dpm(get_defined_vars(), 'get_defined_vars');

if (empty($classes)) {
  $classes = '';
}

if (empty($class)) {
  $class = '';
}
else {
  $classes .= " $class";
}

if (empty($css_id)) {
  $css_id = '';
}
else {
  $css_id = "id=\"$css_id\"";
}


if (!empty($field_layout)) {
  $layout = strtolower($field_layout[0]['value']);
  $classes .= ' layout-' . strtolower($field_layout[0]['value']);
}

if (!empty($field_colour)) {
  $classes .= ' colour-' . strtolower($field_colour[0]['value']);
}

if (isset($field_module_image[0]['uri'])) {
  // the image style depends upon the layout selected
  $image_style = ($layout == 'top-image' ? 'alm_quarterhalf_342x243' : 'alm_quarterfull_342x486');

  $background_image = image_style_url($image_style, $field_module_image[0]['uri']);
  $background_style = "style=\"background-image: url('$background_image');\"";
}
else {
  $background_style = '';
}

?>

<div class="panel-display quarter-module clearfix <?php print $classes; ?>" <?php print $css_id; ?> <?php print $background_style; ?>>

  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12 radix-layouts-content panel-panel">
        <div class="panel-panel-inner">
          <?php print $content['contentmain']; ?>
        </div>
      </div>
    </div>
  </div>

</div><!-- /.boxton -->
