<?php

/**
 * @file
 * This template is used to print a single field in a view.
 *
 * It is not actually used in default Views, as this is registered as a theme
 * function which has better performance. For single overrides, the template is
 * perfectly okay.
 *
 * Variables available:
 * - $view: The view object
 * - $field: The field handler object that can process the input
 * - $row: The raw SQL result that can be used
 * - $output: The processed output that will normally be used.
 *
 * When fetching output from the $row, this construct should be used:
 * $data = $row->{$field->field_alias}
 *
 * The above will guarantee that you'll always get the correct data,
 * regardless of any changes in the aliasing that might happen if
 * the view is modified.
 */
//dpm($row, '$row');

$node = node_load($row->nid);
$link = array(
  '#markup' => l('Open in maps', 'https://maps.google.com/', array(
    'query' => array(
      'q' => token_replace('[node:field_store_address:thoroughfare] [node:field_store_address:premise] [node:field_store_address:locality] [node:field_store_address:administrative-area] [node:field_store_address:postal-code] [node:field_store_address:country]', array('node' => $node))
    ),
    'attributes' => array(
      'target' => '_blank',
      'rel' => 'nofollow',
    ),
  )),
  '#prefix' => '<div class="field field-name-open-in-maps field-label-hidden"><div class="field-items"><div class="field-item">',
  '#suffix' => '</div></div></div>',
  '#weight' => 10,
);

?>
<?php print render($link); ?>
