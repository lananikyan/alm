alm Layouts
===
Responsive Panels layouts for Panopoly and the alm theme.

## Features

* Responsive out of the box
* Works with Panopoly, Panels IPE, and Panelizer
* Easily extendable to support new layouts
* Support for Responsive utility classes (You can easily hide and show panes based on mobile, tablet or desktop viewports)

## Which version?

* For alm 2.x, use alm Layouts 7.x-2.x.
* For alm 3.x, use alm Layouts 7.x-3.x.
