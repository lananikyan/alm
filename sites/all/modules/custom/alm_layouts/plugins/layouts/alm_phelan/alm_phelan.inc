<?php
// Plugin definition
$plugin = array(
  'title' => t('Phelan'),
  'icon' => 'alm-phelan.png',
  'category' => t('ALM'),
  'theme' => 'alm_phelan',
  'regions' => array(
    'header' => t('Header'),
    'column1' => t('First Column'),
    'column2' => t('Second Column'),
  ),
);
