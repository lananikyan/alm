<?php
/**
 * @file
 * Template for Radix Phelan.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display phelan clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="container-fluid">

    <?php if (!empty($content['header'])): ?>
      <div class="row">
        <div class="col-md-12 radix-layouts-header panel-panel <?php if (!empty($region_classes['header'])) { print $region_classes['header']; } ?>"<?php if (isset($region_background_image['header'])) { print ' style="background-image: url(' . $region_background_image['header'] . ');"'; } ?> >
          <div class="panel-panel-inner">
            <?php print $content['header']; ?>
          </div>
        </div>
      </div>
    <?php endif; ?>

    <div class="row">
      <div class="col-md-6 radix-layouts-column1 panel-panel">
        <div class="panel-panel-inner">
          <?php print $content['column1']; ?>
        </div>
      </div>
      <div class="col-md-6 radix-layouts-column2 panel-panel">
        <div class="panel-panel-inner">
          <?php print $content['column2']; ?>
        </div>
      </div>
    </div>
  </div>

</div><!-- /.phelan -->
