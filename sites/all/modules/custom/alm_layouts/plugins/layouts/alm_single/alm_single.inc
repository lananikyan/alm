<?php
// Plugin definition
$plugin = array(
  'title' => t('Single'),
  'icon' => 'alm-single.png',
  'category' => t('ALM'),
  'theme' => 'alm_single',
  'regions' => array(
    'contentmain' => t('Content'),
    'contenthidden' => t('Hidden'),
  ),
);
