<?php
// Plugin definition
$plugin = array(
  'title' => t('Primary'),
  'icon' => 'alm-primary.png',
  'category' => t('ALM'),
  'theme' => 'alm_primary',
  'regions' => array(
    'header' => t('Header'),
    'row1' => t('Row 1'),
    'row2' => t('Row 2'),
    'row3' => t('Row 3'),
    'row4' => t('Row 4'),
    'half' => t('Half Column'),
    'quarter1' => t('Quarter 1 Column'),
    'quarter2' => t('Quarter 2 Column'),
    'footer' => t('Footer'),
  ),
);
