<?php
// Plugin definition
$plugin = array(
  'title' => t('Sutro'),
  'icon' => 'alm-sutro.png',
  'category' => t('ALM'),
  'theme' => 'alm_sutro',
  'regions' => array(
    'header' => t('Header'),
    'row1' => t('Row 1'),
    'row2' => t('Row 2'),
    'row3' => t('Row 3'),
    'row4' => t('Row 4'),
    'column1' => t('First Half'),
    'column2' => t('Second Half'),
    'footer' => t('Footer'),
  ),
);
