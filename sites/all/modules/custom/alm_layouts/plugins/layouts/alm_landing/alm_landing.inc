<?php
// Plugin definition
$plugin = array(
  'title' => t('Landing'),
  'icon' => 'alm-landing.png',
  'category' => t('ALM'),
  'theme' => 'alm_landing',
  'regions' => array(
    'header' => t('Header'),
    'row1' => t('Row 1'),
    'footer' => t('Footer'),
  ),
);
