<?php
/**
 * @file
 * Template for Radix Sutro.
 *
 * Variables:
 * - $css_id: An optional CSS id to use for the layout.
 * - $content: An array of content, each item in the array is keyed to one
 * panel of the layout. This layout supports the following sections:
 */
?>

<div class="panel-display sutro landing clearfix <?php if (!empty($classes)) { print $classes; } ?><?php if (!empty($class)) { print $class; } ?>" <?php if (!empty($css_id)) { print "id=\"$css_id\""; } ?>>

  <div class="container-fluid">

    <?php if (!empty($content['header'])): ?>
    <div class="row header">
      <div class="col-md-12 radix-layouts-header panel-panel <?php if (!empty($region_classes['header'])) { print $region_classes['header']; } ?>"<?php if (isset($region_background_image['header'])) { print ' style="background-image: url(' . $region_background_image['header'] . ');"'; } ?> >
        <div class="panel-panel-inner">
          <?php print $content['header']; ?>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if (!empty($content['row1'])): ?>
    <div class="row content">
      <div class="col-md-12 alm-layouts-header panel-panel <?php if (!empty($region_classes['row1'])) { print $region_classes['row1']; } ?>"<?php if (isset($region_background_image['row1'])) { print ' style="background-image: url(' . $region_background_image['row1'] . ');"'; } ?> >
        <div class="panel-panel-inner">
          <?php print $content['row1']; ?>
        </div>
      </div>
    </div>
    <?php endif; ?>

    <?php if (!empty($content['footer'])): ?>
    <div class="row footer">
      <div class="col-md-12 radix-layouts-footer panel-panel <?php if (!empty($region_classes['footer'])) { print $region_classes['footer']; } ?>"<?php if (isset($region_background_image['footer'])) { print ' style="background-image: url(' . $region_background_image['footer'] . ');"'; } ?> >
        <div class="panel-panel-inner">
          <?php print $content['footer']; ?>
        </div>
      </div>
    </div>
  </div>
  <?php endif; ?>

</div><!-- /.sutro -->
