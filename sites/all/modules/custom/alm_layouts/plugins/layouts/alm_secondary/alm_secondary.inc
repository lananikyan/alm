<?php
// Plugin definition
$plugin = array(
  'title' => t('Secondary'),
  'icon' => 'alm-secondary.png',
  'category' => t('ALM'),
  'theme' => 'alm_secondary',
  'regions' => array(
    'header' => t('Header'),
    'sidebar' => t('Content Sidebar'),
    'contentmain' => t('Content'),
  ),
);
