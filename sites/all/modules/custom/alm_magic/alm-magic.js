/**
 * Add event tracking to competition links
 */
(function ($) {

    Drupal.behaviors.AlmMagicTrackCompetitionLinks = {
        attach: function (context, settings) {
            // Code to be run on page load, and
            // on ajax load added here

            $('.page-competitions .field-name-field-call-to-action-link a', context).click(function (event) {
                // new analytics.js method
                // use transport: beacon to track an event just before a user navigates away from your site, without
                // delaying the navigation
                ga('send', 'event', {
                    eventCategory: 'Competition',
                    eventAction: 'Enter',
                    eventLabel: 'Visit Competition',
                    transport: 'beacon'
                });

            });

        }
    };

}(jQuery));

