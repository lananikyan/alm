CONTENT OF THIS FILE
--------------------

* About this module
* Configuration
* Usage

ABOUT THIS MODULE
-----------------
This module extends the functionnalities of the features module.
It adds drush commands to automatically add all new components of a specific
type into an existing features module. It is an __update of the scope of a
features module__.
(Scope is the actual list of features components present in a module.)

This module suggests spliting all your features export __by component type__.

For example, you created the features-module `feat_image_styles` which is
designed to contains ALL the "image styles" of your website. During a new
release you add 2 new image styles `my_new_style_1` and `my_new_style_2`.
Executing commands: `drush fus feat_image_styles` will add this  2 new
components into the `feat_image_styles`.


CONFIGURATION
-------------
the module provide 3 drush commands and one admin screen:

### Admin screen ###
admin/structure/features/demonz-features
Let you specify WHICH order you want to apply when running a features-update-scope-all.
This is usefull to prevent some feature modules integrating some new unwanted
components by dependencies that you may have wanted to see integrated in another
module.
Example: preventing a "views features module" grabbing some new image components
(image styles) you wanted integrated in a specific "image styles features module".
In this case you will have to define the image styles features module" BEFORE
the "views features module".

### Drushs commands ###
* `drush features-update-all-ordered`:
Update all feature modules on your site using the order provided (fu-all-o, fuao)
* `drush features-update-scope [features_module_name]`:
Update the scope of a feature module on your site. (fus)
* `drush features-update-scope-all`:
Update the scope of all feature modules. (fus-all, fusa)


USAGE
-----

### Define a new features module ###
1. create a features module including 1 component. (or use an existing one)
2. edit the `.info` file:
    1. Define the component type(s) of this module: add the line:
    `features_update_component_type[] = {component-type}`
    (you may add several lines if you want to group together several component type.)
    2. Define exclusions (optional): add the line(s):
    `features_update_exclude_component[{component-type}][] = {component-name}`..
    you can add pattern generic name: `cache_*`
3. save the `.info` file
4. Visit the admin screen (admin/structure/features/demonz-features) and add
your module in the list.

### Update your features modules ###
* To update ALL your existing features Run the command:
`drush -y fusa --update-content`
* to update ONE features module Run the command:
`drush -y fus [features_module_name]`; drush -y fu [features_module_name]`
