/**
 * Add event tracking to webform submit
 */
(function ($) {

    Drupal.behaviors.AlmWebformTrackSignup = {
        attach: function (context, settings) {
            // Code to be run on page load, and
            // on ajax load added here

            $('form.webform-client-form input.webform-submit', context).click(function (event) {
                // old ga.js method
                // _gaq.push(['_trackEvent', 'Newsletter', 'Sign Up', 'Info', , false]);
                
                // new analytics.js method
                // use transport: beacon to track an event just before a user navigates away from your site, without
                // delaying the navigation
                ga('send', 'event', {
                    eventCategory: 'Newsletter',
                    eventAction: 'Sign Up',
                    eventLabel: 'Info',
                    transport: 'beacon'
                });

            });

        }
    };

}(jQuery));
