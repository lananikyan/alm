/**
 * Created by demonz on 2/11/2015.
 */
(function ($) {

    // Add event tracking to store name clicks
    Drupal.behaviors.AlmStoreLocatorTrackStoreVisit = {
        attach: function (context, settings) {
            // Code to be run on page load, and
            // on ajax load added here

            $('.field-name-take-me-there a', context).click(function (event) {
                // old ga.js method
                // _gaq.push(['_trackEvent', 'Visit Store', 'Instore', 'Click',, false]);

                // new analytics.js method
                // use transport: beacon to track an event just before a user navigates away from your site, without
                // delaying the navigation
                ga('send', 'event', {
                    eventCategory: 'Visit Store',
                    eventAction: 'Instore',
                    eventLabel: 'Click',
                    transport: 'beacon'
                });

            });

        }
    };

    // Override the behavior of ip_geoloc module (ip_geoloc_gmap_multi_loc.js)
    // (a bit hacky, but it will do for now...)
    Drupal.behaviors.addGMapMultiLocation = {
        attach: function (context, settings) {

            if (typeof (google) !== 'object') {
                // When not connected to Internet.
                return;
            }
            // Create map as a global, see [#1954876].
            // As we can have multiple maps on the same page, maps is an array.
            maps = [];
            mapBounds = [];
            var imageExt = '.png';
            var infoBubble;
            //var infoWindow = new google.maps.InfoWindow();

            var backgroundColor = '#000';
            var bodyRef = jQuery('body');
            if (bodyRef.hasClass('domain-cellarbrations-com-au')) {
                backgroundColor = '#1b242b';
            } else if (bodyRef.hasClass('domain-liquor-iga-com-au')) {
                backgroundColor = '#ed1b24';
            } else if (bodyRef.hasClass('domain-thebottle-o-com-au')) {
                backgroundColor = '#006f3c';
            } else if (bodyRef.hasClass('domain-duncans-com-au')) {
                backgroundColor = '#1d1d1d';
            }


            //google.maps.event.addListener(infoWindow, 'domready', function () {
            //    // Custom code to fully customize infowindows:

            //    var iwOuter = jQuery('.gm-style-iw');
            //    //iwOuter.css("border", "10px solid red");

            //    /* The DIV we want to change is above the .gm-style-iw DIV.
            //     * So, we use jQuery and create a iwBackground variable,
            //     * and took advantage of the existing reference to .gm-style-iw for the previous DIV with .prev().
            //     */
            //    var iwBackground = iwOuter.prev();

            //    // Remove the background shadow DIV
            //    iwBackground.children(':nth-child(2)').css({ 'display': 'none' });

            //    // Remove the white background DIV
            //    iwBackground.children(':nth-child(4)').css({ 'display': 'none' });

            //    // Moves the infowindow 115px to the right.
            //    //iwOuter.parent().parent().css({ left: '115px' });

            //    // Moves the shadow of the arrow 76px to the left margin 
            //    iwBackground.children(':nth-child(1)').attr('style', function (i, s) { return s + 'left: 76px !important;' });

            //    // Moves the arrow 76px to the left margin 
            //    iwBackground.children(':nth-child(3)').attr('style', function (i, s) { return s + 'left: 76px !important;' });

            //    // Changes the desired color for the tail outline.
            //    // The outline of the tail is composed of two descendants of div which contains the tail.
            //    // The .find('div').children() method refers to all the div which are direct descendants of the previous div. 
            //    iwBackground.children(':nth-child(3)').find('div').children().css({ 'box-shadow': 'none', 'background-color': '#1b242b', 'z-index': '1' });

            //    iwOuter.find('.balloon').parent().css('overflow', 'hidden');

            //    var iwCloseBtn = iwOuter.next();
            //    iwCloseBtn.css({ 'display': 'none' });

            //});

            $(settings, context).each(function () {

                for (var m in settings.ipGeoloc) {

                    if (isNaN(m)) {
                        continue;
                    }
                    var divId = settings.ipGeoloc[m].ip_geoloc_multi_location_map_div;
                    var mapDiv = document.getElementById(divId);
                    if (!mapDiv) {
                        continue;
                    }
                    var mapOptions = settings.ipGeoloc[m].ip_geoloc_multi_location_map_options;
                    if (!mapOptions) {
                        alert(Drupal.t('IPGV&M: syntax error in Google map options.'));
                    }
                    maps[m] = new google.maps.Map(mapDiv, mapOptions);
                    // A map must have a type, a zoom and a center or nothing will show.
                    if (!maps[m].getMapTypeId()) {
                        maps[m].setMapTypeId(google.maps.MapTypeId.ROADMAP);
                    }
                    if (!maps[m].getZoom()) {
                        maps[m].setZoom(1);
                    }
                    // Check for the special non-Google fixed-center option we provide.
                    if (mapOptions.centerLat && mapOptions.centerLng) {
                        maps[m].setCenter(new google.maps.LatLng(mapOptions.centerLat, mapOptions.centerLng));
                    }
                    var locations = ip_geoloc_locations[divId];
                    var visitorMarker = settings.ipGeoloc[m].ip_geoloc_multi_location_visitor_marker;
                    var centerOption = settings.ipGeoloc[m].ip_geoloc_multi_location_center_option;
                    var use_gps = settings.ipGeoloc[m].ip_geoloc_multi_location_visitor_location_gps;
                    var markerDirname = settings.ipGeoloc[m].ip_geoloc_multi_location_marker_directory;
                    var markerWidth = settings.ipGeoloc[m].ip_geoloc_multi_location_marker_width;
                    var markerHeight = settings.ipGeoloc[m].ip_geoloc_multi_location_marker_height;
                    var markerAnchor = settings.ipGeoloc[m].ip_geoloc_multi_location_marker_anchor;
                    var markerColor = settings.ipGeoloc[m].ip_geoloc_multi_location_marker_default_color;

                    var defaultMarkerWidth = markerWidth;
                    var defaultMarkerHeight = markerHeight;

                    var defaultPinImage = !markerColor ? null : new google.maps.MarkerImage(
                        markerDirname + '/' + markerColor + imageExt,
                        new google.maps.Size(markerWidth, markerHeight),
                        // Origin
                        new google.maps.Point(0, 0),
                        // Anchor
                        new google.maps.Point((markerWidth / 2), markerAnchor),
                        // scaledSize
                        new google.maps.Size(markerWidth, markerHeight));

                    var center = null;
                    mapBounds[m] = new google.maps.LatLngBounds();
                    for (var key in locations) {
                        var br = locations[key].balloon_text.indexOf('<br/>');
                        var mouseOverText = (br > 0) ? locations[key].balloon_text.substring(0, br) : locations[key].balloon_text.trim();
                        // Strip out HTML tags.
                        try {
                            // Add <span> to make sure there are tags.
                            mouseOverText = jQuery('<span>' + mouseOverText + '</span>').text();
                        }
                        catch (err) {
                        }
                        if (mouseOverText.length === 0) {
                            mouseOverText = Drupal.t('Location #@i', {'@i': parseInt(key) + 1});
                        }

                        var position = new google.maps.LatLng(locations[key].latitude, locations[key].longitude);
                        mapBounds[m].extend(position);
                        if (!center && centerOption === 1 /* center on 1st location */) {
                            maps[m].setCenter(position);
                            center = position;
                        }
                        var pinImage = defaultPinImage;

                        // Custom code: first marker is alm-marker-big.png and it is 2x bigger:
                        if (key == 0) {
                            markerWidth = 2 * defaultMarkerWidth;
                            markerHeight = 2 * defaultMarkerHeight;
                        }
                        else {
                            markerWidth = defaultMarkerWidth;
                            markerHeight = defaultMarkerHeight;
                        }
                        // end custom code

                        if (locations[key].marker_color) {
                            pinImage = new google.maps.MarkerImage(
                                markerDirname + '/' + locations[key].marker_color + imageExt,
                                new google.maps.Size(markerWidth, markerHeight),
                                // Origin
                                new google.maps.Point(0, 0),
                                // Anchor
                                // make sure to adjust marker anchor point to equal the bottom edge of icon (full height)
                                new google.maps.Point((markerWidth / 2), markerHeight),
                                // scaledSize
                                new google.maps.Size(markerWidth, markerHeight)
                            );
                        }
                        var marker = new google.maps.Marker({
                            map: maps[m],
                            icon: pinImage, /*shadow: shadowImage,*/
                            position: position,
                            title: mouseOverText,
                            optimized: false
                        });

                        var balloonText = '<div class="balloon">' + locations[key].balloon_text + '</div>';
                        addMarkerBalloon(maps[m], marker, balloonText);

                        if (locations[key].open) {
                            //infoWindow = new google.maps.InfoWindow({ content: balloonText, maxWidth: 200 });
                            //infoWindow.open(maps[m], marker);
                            openBubble(map, marker, balloonText);
                        }
                    }
                    if (centerOption === 3 && locations.length > 0) {
                        // Auto-box: ensure that all markers are visible on the initial map.
                        maps[m].fitBounds(mapBounds[m]);
                        //maps[m].panToBounds(mapBounds[m]);
                    }

                    if (visitorMarker || centerOption === 2 /* center on visitor */ || centerOption === 6 /* auto-box incl. visitor*/) {
                        // Retrieve visitor's location, fall back on supplied location, if not found.
                        if (use_gps && navigator.geolocation) {
                            navigator.geolocation.getCurrentPosition(handleMapCenterAndVisitorMarker1, handlePositionError, {enableHighAccuracy: true});
                        }
                        else {
                            // No HTML5 retrieval: use supplied visitor lat/lng.
                            var latLng = settings.ipGeoloc[m].ip_geoloc_multi_location_center_latlng;
                            if (latLng) {
                                handleMapCenterAndVisitorMarker2(latLng[0], latLng[1]);
                            }
                        }
                    }
                }
            });

            function handleMapCenterAndVisitorMarker1(visitorPosition) {
                handleMapCenterAndVisitorMarker2(visitorPosition.coords.latitude, visitorPosition.coords.longitude);
            }

            // Center all maps and add the special visitor marker on all maps too.
            function handleMapCenterAndVisitorMarker2(latitude, longitude) {
                var visitorPosition = new google.maps.LatLng(latitude, longitude);
                for (var m in maps) {
                    if (isNaN(m)) continue;
                    if (settings.ipGeoloc[m].ip_geoloc_multi_location_visitor_marker) {
                        var p = '(' + latitude + ', ' + longitude + ')';
                        showSpecialMarker(m, visitorPosition, Drupal.t('Your approximate location') + "\n" + p);
                    }
                    if (settings.ipGeoloc[m].ip_geoloc_multi_location_center_option === 2) {
                        maps[m].setCenter(visitorPosition);
                    }
                    else if (settings.ipGeoloc[m].ip_geoloc_multi_location_center_option === 6) {
                        // Autobox including visitor location.
                        mapBounds[m].extend(visitorPosition);
                        maps[m].fitBounds(mapBounds[m]);
                    }
                }
            }

            function openBubble(map, marker, infoText) {

                if (infoBubble) {
                    infoBubble.close();
                }

                infoBubble = new InfoBubble({
                    map: map,
                    content: infoText,
                    position: marker.getPosition(),
                    shadowStyle: 0,
                    padding: 20,
                    backgroundColor: backgroundColor,
                    borderRadius: 0,
                    arrowSize: 10,
                    borderWidth: 0,
                    borderColor: 'transparent',
                    disableAutoPan: false,
                    hideCloseButton: false,
                    arrowPosition: 30,
                    backgroundClassName: 'infobubble',
                    arrowStyle: 0,
                    //maxWidth: 450,
                    minWidth: 350,
                    minHeight: 250,
                    //closeSrc: 'http://www.demonzmedia.com/wp-content/themes/demonz/img/logo.svg'
                    //pixelOffset: new google.maps.Size(130, 120),
                    //maxHeight: 500

                });

                infoBubble.open(map, marker);

                //var oldLatLng = marker.getPosition();
                //var newLatLng = new google.maps.LatLng(oldLatLng.lat() , oldLatLng.lng())
                //map.panTo(newLatLng);


            }

            function addMarkerBalloon(map, marker, infoText) {
                google.maps.event.addListener(marker, 'click', function (event) {
                    openBubble(map, marker, infoText);
                });


                //google.maps.event.addListener(marker, 'click', function(event) {
                //    if (infoWindow) {
                //        infoWindow.close();
                //    }
                //    infoWindow.setOptions({
                //        content: infoText,
                //        // See [#1777664].
                //        maxWidth: 200
                //    });
                //    infoWindow.open(map, marker);
                //});
            }

            function showSpecialMarker(m, position, mouseOverText) {
                var specialMarker;
                var visitorMarker = settings.ipGeoloc[m].ip_geoloc_multi_location_visitor_marker;
                if (visitorMarker === true) {
                    specialMarker = new google.maps.Marker({
                        map: maps[m],
                        position: position,
                        title: mouseOverText
                    });
                }
                else {
                    // Interpret value of visitorMarker as the marker RGB color, for
                    // instance "00FF00" is bright green.
                    var pinColor = visitorMarker;
                    // Use a standard character like "x", or for a dot use "%E2%80%A2".
                    var pinChar = "%E2%80%A2";
                    // Black.
                    var textColor = "000000";
                    // Note: cannot use https: here...
                    var pinImage = new google.maps.MarkerImage("http://chart.apis.google.com/chart?chst=d_map_pin_letter&chld=" + pinChar + "|" + pinColor + "|" + textColor,
                        new google.maps.Size(21, 34), new google.maps.Point(0, 0), new google.maps.Point(10, 34));
                    specialMarker = new google.maps.Marker({
                        map: maps[m],
                        position: position,
                        zIndex: 9999,
                        title: mouseOverText,
                        icon: pinImage
                        //shadow: shadowImage
                    });
                }
                addMarkerBalloon(maps[m], specialMarker, mouseOverText);
            }

            // Fall back on IP address lookup, for instance when user declined to share location.
            function handlePositionError(error) {
                var latLng = settings.ipGeoloc[0].ip_geoloc_multi_location_center_latlng;
                if (latLng) {
                    handleMapCenterAndVisitorMarker2(latLng[0], latLng[1]);
                }
                else {
                    for (var m in maps) {
                        // If centering involving the visitor location was requested, but we
                        // don't have one, then auto-box the remaining markers.
                        if (settings.ipGeoloc[m].ip_geoloc_multi_location_center_option === 2 || settings.ipGeoloc[m].ip_geoloc_multi_location_center_option === 6) {
                            maps[m].fitBounds(mapBounds[m]);
                        }
                    }
                }
            }
        }
    };


})(jQuery);
