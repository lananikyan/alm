<?php 
function drifter_submit_form($form, &$form_state) {
  $form =  array();
  
  $form['#attached']['css'] = array(
  drupal_get_path('module', 'drifter_form') . '/drifter_form.css',
);
  $form['description'] = array(
    '#markup' => "<h3>Win lunch for you and three friends at rosehill garden's 'winning post' restaurant</h3><div class='description-custom'><img src='/sites/default/files/www.png'><p>Simply hold onto your Cellarbrations receipt showing your purchase of a bottle from the new Sterling range, and then enter your details below for your chance to win lunch for you and three friends on Saturday 18th November, 2017</p><p class='italic'>Please note, entry is only open to NSW residents who are 18 years and over.</p></div>"
  );

  $form['first_name'] = array(
    '#type' => 'textfield',
    '#title' => t('First Name'),
    '#default_value' =>'',
    '#required' => TRUE,
    '#attributes' => array('placeholder' => "First name"),
  );

  $form['last_name'] = array(
    '#type' => 'textfield',
    '#title' => t('Last Name'),
    '#default_value' =>'',
    '#required' => TRUE,
    '#attributes' => array('placeholder' => "Last name"),
  );

  $form['contact_number'] = array(
    '#type' => 'textfield',
    '#title' => t('Contact Number'),
    '#default_value' =>'',
    '#required' => TRUE,
    '#attributes' => array('placeholder' => "Contact Number (used to contact winners only)"),
  );

  $form['email'] = array(
    '#type' => 'email',
    '#title' => t('Email'),
    '#default_value' =>'',
    '#required' => TRUE,
    '#attributes' => array('placeholder' => "Email"),
  );

  $form['sign_up'] = array(
    '#type' => 'checkbox',
    '#title' => t('Sign up to hear more'),
    '#default_value' =>'',
  );

  $form['reciept'] = array(
    '#type' => 'checkbox',
    '#title' => t('Yes I do have a copy of my receipt'),
    '#default_value' =>'',
  );

  $form['preamble'] = array(
    '#markup' => '<p>By clicking submit, you agree to the <a href="https://www.cellarbrations.com.au/articles/cb-spring-racing-terms-conditions-conditions-entry" target="_blank">terms and conditions</a><br /></p>'
  );

  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Enter Now"),
    '#submit' => array('drifter_submit_form_submit'),
  );

  return $form;

}
/*
* Form submit action
*
*/

function drifter_submit_form_submit($form, &$form_state) {
  db_insert("data_submission")
      ->fields(array(
          'first_name' => $form_state['values']['first_name'],
          'last_name' => $form_state['values']['last_name'],
          'contact' => $form_state['values']['contact_number'],
          'sign_up' => $form_state['values']['sign_up'],
          'receipt' => $form_state['values']['reciept'],
          'state' => 'NSW',
      ))
    ->execute();
    //$_SESSION['sterling_success'] = TRUE;
  drupal_goto('/competitions/sterling/success');
}

function drifter_success_form() {
  /*if (isset($_SESSION['sterling_success']) && $_SESSION['sterling_success']) {
    unset($_SESSION['sterling_success']);*/
    $form = array();
    $form['#attached']['css'] = array(
      drupal_get_path('module', 'drifter_form') . '/drifter_form.css',
    );
    $form['success'] = array(
      '#markup' => '<div class="success-submision-form"><p>Thank you for your entry and good luck!</p></div>',
    );

    $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t("Back To Home"),
    '#submit' => array('drifter_success_form_submit'),
    );

    return $form;
  /*}else {
    drupal_goto('<front>');
  }*/ 
}

function drifter_success_form_submit($form, &$form_state) {
  drupal_goto('<front>');
}
