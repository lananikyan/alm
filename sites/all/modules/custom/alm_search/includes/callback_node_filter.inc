<?php

/**
 * Represents a data alteration that exclude some node from index.
 */
class ExcludeNodeAlter extends SearchApiAbstractAlterCallback {

  public function supportsIndex(SearchApiIndex $index) {
    return $index->getEntityType() === 'node';
  }

  public function configurationForm() {
    $options = array();

    $form = array(
      'exclude_nids' => array(
        '#type' => 'textfield',
        '#title' => t('Which nodes (nid) should be NOT indexed?'),
        '#default_value' => isset($this->options['exclude_nids']) ? $this->options['exclude_nids'] : '',
        '#description' => t('Space-separated list of nid to exclude'),
      ),
    );

    return $form;
  }

  public function alterItems(array &$items) {
    if (!empty($items) && isset($this->options['exclude_nids']) && !empty($this->options['exclude_nids'])) {
      // Get the nid in an array:
      $nids = explode(' ', $this->options['exclude_nids']);

      // Remove items which shouldn't be indexed.
      foreach ($items as $id => $item) {
        if (in_array($item->nid, $nids)) {
            unset($items[$id]);
        }
      }
    }
  }
}
