<?php

/**
 * Implements hook_rules_condition_info().
 */
function workbench_extra_rules_condition_info() {
  $items = array();

  $items['contents_state_changed'] = array(
    'group' => t("Node"),
    'label' => t("Content's moderation state REALLY changed"),
    'base' => 'workbench_extra_rules_condition_contents_state_changed',
    'parameter' => array(
      'node' => array('type' => 'node', 'label' => t("Content")),
    ),
    'access callback' => 'rules_node_integration_access',
  );

  return $items;
}

/**
 * Condition: Check if workbench moderation state REALLY changed.
 *
 * @param $node
 *   A node object
 *
 * @return
 *  TRUE/FALSE depending on if the nodes previous state matches nodes current state.
 */
function workbench_extra_rules_condition_contents_state_changed($node) {
  if (!is_object($node)) {
    return FALSE;
  }

  if ($node->workbench_moderation['current']->from_state == $node->workbench_moderation['current']->state) {
    return FALSE;
  }

  return TRUE;
}

