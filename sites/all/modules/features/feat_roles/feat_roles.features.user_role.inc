<?php
/**
 * @file
 * feat_roles.features.user_role.inc
 */

/**
 * Implements hook_user_default_roles().
 */
function feat_roles_user_default_roles() {
  $roles = array();

  // Exported role: administrator.
  $roles['administrator'] = array(
    'name' => 'administrator',
    'weight' => 2,
  );

  // Exported role: moderator.
  $roles['moderator'] = array(
    'name' => 'moderator',
    'weight' => 3,
  );

  // Exported role: site admin.
  $roles['site admin'] = array(
    'name' => 'site admin',
    'weight' => 4,
  );

  // Exported role: store owner.
  $roles['store owner'] = array(
    'name' => 'store owner',
    'weight' => 5,
  );

  return $roles;
}
