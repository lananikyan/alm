<?php
/**
 * @file
 * feat_images.features.inc
 */

/**
 * Implements hook_image_default_styles().
 */
function feat_images_image_default_styles() {
  $styles = array();

  // Exported image style: alm_bloglarge_845x475.
  $styles['alm_bloglarge_845x475'] = array(
    'label' => 'Blog large image (845x475)',
    'effects' => array(
      24 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 845,
          'height' => 475,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_bloglarge_845x475',
        ),
        'weight' => 0,
      ),
      13 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 845,
          'height' => 475,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_blogthumb_79x52.
  $styles['alm_blogthumb_79x52'] = array(
    'label' => 'Blog thumbnail image (79x52)',
    'effects' => array(
      14 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 79,
          'height' => 52,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_carousel_1366x657.
  $styles['alm_carousel_1366x657'] = array(
    'label' => 'Carousel image (1366x585)',
    'effects' => array(
      18 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1366,
          'height' => 585,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_carousel_1366x657',
        ),
        'weight' => 0,
      ),
      19 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 585,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: alm_carouselmobile_640x460.
  $styles['alm_carouselmobile_640x460'] = array(
    'label' => 'Carousel mobile (640x460)',
    'effects' => array(
      17 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 640,
          'height' => 460,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_carouselmobile_640x460',
        ),
        'weight' => 0,
      ),
      15 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 640,
          'height' => 460,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_carouselogo_230x80.
  $styles['alm_carouselogo_230x80'] = array(
    'label' => 'Carousel logo (230x80)',
    'effects' => array(
      20 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 230,
          'height' => 80,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_carouselogo_230x80',
        ),
        'weight' => 0,
      ),
      11 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 230,
          'height' => 80,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_carouselproduct_133x400.
  $styles['alm_carouselproduct_133x400'] = array(
    'label' => 'Carousel product (133x400)',
    'effects' => array(
      21 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 133,
          'height' => 400,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_carouselproduct_133x400',
        ),
        'weight' => 0,
      ),
      12 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 133,
          'height' => 400,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_drinkresponsibly_230x30.
  $styles['alm_drinkresponsibly_230x30'] = array(
    'label' => 'Drink Responsibly image (230x30)',
    'effects' => array(
      22 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 230,
          'height' => 30,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_drinkresponsibly_230x30',
        ),
        'weight' => 0,
      ),
      23 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 230,
          'height' => 30,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: alm_halfpage_684x495.
  $styles['alm_halfpage_684x495'] = array(
    'label' => 'Half page image (684x495)',
    'effects' => array(
      25 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 684,
          'height' => 495,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_halfpage_684x495',
        ),
        'weight' => 0,
      ),
      6 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 684,
          'height' => 495,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_header_1366x351.
  $styles['alm_header_1366x351'] = array(
    'label' => 'Header image (1366x351)',
    'effects' => array(
      27 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 1366,
          'height' => 351,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_header_1366x351',
        ),
        'weight' => 0,
      ),
      28 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 1366,
          'height' => 351,
        ),
        'weight' => 2,
      ),
    ),
  );

  // Exported image style: alm_quarterfull_342x486.
  $styles['alm_quarterfull_342x486'] = array(
    'label' => 'Quarter full image (342x495)',
    'effects' => array(
      26 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 342,
          'height' => 495,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_quarterfull_342x486',
        ),
        'weight' => 0,
      ),
      5 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 342,
          'height' => 495,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_quarterhalf_342x243.
  $styles['alm_quarterhalf_342x243'] = array(
    'label' => 'Quarter half image (342x243)',
    'effects' => array(
      10 => array(
        'name' => 'manualcrop_crop_and_scale',
        'data' => array(
          'width' => 342,
          'height' => 243,
          'upscale' => 0,
          'respectminimum' => 1,
          'onlyscaleifcrop' => 1,
          'style_name' => 'alm_quarterhalf_342x243',
        ),
        'weight' => 0,
      ),
      4 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 342,
          'height' => 243,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_specialsgrid_297x519.
  $styles['alm_specialsgrid_297x519'] = array(
    'label' => 'Specials grid (297x519)',
    'effects' => array(
      9 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 297,
          'height' => 519,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: alm_specialspage_530x535.
  $styles['alm_specialspage_530x535'] = array(
    'label' => 'Specials page (530x535)',
    'effects' => array(
      8 => array(
        'name' => 'image_scale_and_crop',
        'data' => array(
          'width' => 530,
          'height' => 535,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: full_width_1366x_.
  $styles['full_width_1366x_'] = array(
    'label' => 'Full width (1355xY)',
    'effects' => array(
      29 => array(
        'name' => 'image_scale',
        'data' => array(
          'width' => 1366,
          'height' => '',
          'upscale' => 0,
        ),
        'weight' => 1,
      ),
    ),
  );

  // Exported image style: landscape_16_9.
  $styles['landscape_16_9'] = array(
    'label' => 'Landscape (16:9) (deprecated) (deprecated) (deprecated) (deprecated) (deprecated)',
    'effects' => array(
      2 => array(
        'name' => 'manualcrop_crop',
        'data' => array(
          'width' => 410,
          'height' => 230,
          'keepproportions' => 1,
          'reuse_crop_style' => '',
          'style_name' => 'landscape_16_9',
        ),
        'weight' => 0,
      ),
    ),
  );

  // Exported image style: portrait_16_9.
  $styles['portrait_16_9'] = array(
    'label' => 'Portrait (9:16) (deprecated) (deprecated) (deprecated) (deprecated) (deprecated)',
    'effects' => array(
      1 => array(
        'name' => 'manualcrop_crop',
        'data' => array(
          'width' => 230,
          'height' => 410,
          'keepproportions' => 1,
          'reuse_crop_style' => '',
          'style_name' => 'portrait_16_9',
        ),
        'weight' => 0,
      ),
    ),
  );

  return $styles;
}
