<?php
/**
 * @file
 * feat_fields.field_group.inc
 */

/**
 * Implements hook_field_group_info().
 */
function feat_fields_field_group_info() {
  $export = array();

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_copy|node|blog_article|teaser';
  $field_group->group_name = 'group_module_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'blog_article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fields & copy',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_article_tags',
      2 => 'title',
      3 => 'node_link',
      4 => 'post_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Fields & copy',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-copy field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_copy|node|blog_article|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_copy|node|double|default';
  $field_group->group_name = 'group_module_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'double';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fields & copy',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_module_preheader',
      2 => 'field_call_to_action_link',
      3 => 'field_tags',
      4 => 'field_module_heading',
      5 => 'double_sharethis',
      6 => 'post_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Fields & copy',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-copy field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_copy|node|double|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_copy|node|half|default';
  $field_group->group_name = 'group_module_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'half';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fields & copy',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_module_preheader',
      2 => 'field_call_to_action_link',
      3 => 'field_module_heading',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Fields & copy',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-copy field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_copy|node|half|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_copy|node|quarter|default';
  $field_group->group_name = 'group_module_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'quarter';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fields & copy',
    'weight' => '1',
    'children' => array(
      0 => 'body',
      1 => 'field_call_to_action_link',
      2 => 'field_module_preheader',
      3 => 'field_module_heading',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Fields & copy',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-copy field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_copy|node|quarter|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_copy|node|standard_article|teaser';
  $field_group->group_name = 'group_module_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'standard_article';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fields & copy',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'field_article_tags',
      2 => 'title',
      3 => 'node_link',
      4 => 'post_date',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Fields & copy',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-copy field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_copy|node|standard_article|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_copy|node|store|teaser';
  $field_group->group_name = 'group_module_copy';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'store';
  $field_group->mode = 'teaser';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Fields & copy',
    'weight' => '0',
    'children' => array(
      0 => 'body',
      1 => 'who_we_are_preheader',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Fields & copy',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-copy field-group-div',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_copy|node|store|teaser'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_overlay|node|carousel|default';
  $field_group->group_name = 'group_module_overlay';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'carousel';
  $field_group->mode = 'default';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Overlay',
    'weight' => '2',
    'children' => array(
      0 => 'body',
      1 => 'field_module_preheader',
      2 => 'field_call_to_action_link',
      3 => 'field_module_image',
      4 => 'field_carousel_disclaimer',
      5 => 'field_carousel_logo',
      6 => 'field_module_heading',
      7 => 'field_drink_responsibility_image',
    ),
    'format_type' => 'div',
    'format_settings' => array(
      'label' => 'Overlay',
      'instance_settings' => array(
        'id' => '',
        'classes' => 'group-module-overlay field-group-fieldset',
        'description' => '',
        'show_label' => '0',
        'label_element' => 'h3',
        'effect' => 'none',
        'speed' => 'fast',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_overlay|node|carousel|default'] = $field_group;

  $field_group = new stdClass();
  $field_group->disabled = FALSE; /* Edit this to true to make a default field_group disabled initially */
  $field_group->api_version = 1;
  $field_group->identifier = 'group_module_overlay|node|carousel|form';
  $field_group->group_name = 'group_module_overlay';
  $field_group->entity_type = 'node';
  $field_group->bundle = 'carousel';
  $field_group->mode = 'form';
  $field_group->parent_name = '';
  $field_group->data = array(
    'label' => 'Overlay',
    'weight' => '4',
    'children' => array(
      0 => 'body',
      1 => 'field_module_preheader',
      2 => 'field_call_to_action_link',
      3 => 'field_carousel_layout',
      4 => 'field_module_image',
      5 => 'field_carousel_disclaimer',
      6 => 'field_carousel_logo',
      7 => 'field_drink_responsibility_logo',
      8 => 'field_module_heading',
      9 => 'field_colour',
      10 => 'field_drink_responsibility_image',
    ),
    'format_type' => 'fieldset',
    'format_settings' => array(
      'label' => 'Overlay',
      'instance_settings' => array(
        'required_fields' => 1,
        'classes' => 'group-module-overlay field-group-fieldset',
        'description' => '',
      ),
      'formatter' => 'open',
    ),
  );
  $export['group_module_overlay|node|carousel|form'] = $field_group;

  return $export;
}
