<?php
/**
 * @file
 * feat_nodes.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function feat_nodes_ctools_plugin_api($module = NULL, $api = NULL) {
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_node_info().
 */
function feat_nodes_node_info() {
  $items = array(
    'blog_article' => array(
      'name' => t('Blog article'),
      'base' => 'node_content',
      'description' => t('Houses many different types of content due to its flexible nature and the different functions and outputs allowed by the full WYSIWYG.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'brand' => array(
      'name' => t('Brand'),
      'base' => 'node_content',
      'description' => t('The brand listing module is automatically populated as an alphabetised listing from this content.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'carousel' => array(
      'name' => t('Carousel item'),
      'base' => 'node_content',
      'description' => t('Showcase internal content as well as links to hero brands and promotions.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'catalogue' => array(
      'name' => t('Catalogue'),
      'base' => 'node_content',
      'description' => t('Duncans and IGA Liquor will only  the PDF version of the specials catalogue on their specials pages.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'double' => array(
      'name' => t('Double item'),
      'base' => 'node_content',
      'description' => t('A double module is a combination of two elements, an image and text.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'half' => array(
      'name' => t('Half item'),
      'base' => 'node_content',
      'description' => t('A half module allows for responsive rich content, imagery and copy.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'quarter' => array(
      'name' => t('Quarter item'),
      'base' => 'node_content',
      'description' => t('A flexible quarter module that can be used throughout the site. Options: text only, text and top image, text and side image or image only.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'standard_article' => array(
      'name' => t('Standard article'),
      'base' => 'node_content',
      'description' => t('Houses many different types of content due to its flexible nature and the different functions and outputs allowed by the full WYSIWYG.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'store' => array(
      'name' => t('Store'),
      'base' => 'node_content',
      'description' => t('An item representing an individual store.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
    'webform' => array(
      'name' => t('Webform'),
      'base' => 'node_content',
      'description' => t('Create a new form or questionnaire accessible to users. Submission results and statistics are recorded and accessible to privileged users.'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  drupal_alter('node_info', $items);
  return $items;
}
