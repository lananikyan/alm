<?php
/**
 * @file
 * feat_beans.bean.inc
 */

/**
 * Implements hook_bean_admin_ui_types().
 */
function feat_beans_bean_admin_ui_types() {
  $export = array();

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'free_text_block';
  $bean_type->label = 'Free text';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['free_text_block'] = $bean_type;

  $bean_type = new stdClass();
  $bean_type->disabled = FALSE; /* Edit this to true to make a default bean_type disabled initially */
  $bean_type->api_version = 5;
  $bean_type->name = 'link_block';
  $bean_type->label = 'Link';
  $bean_type->options = '';
  $bean_type->description = '';
  $export['link_block'] = $bean_type;

  return $export;
}
