<?php
/**
 * @file
 * feat_feeds.feeds_importer_default.inc
 */

/**
 * Implements hook_feeds_importer_default().
 */
function feat_feeds_feeds_importer_default() {
  $export = array();

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'blog_import';
  $feeds_importer->config = array(
    'name' => 'Blog import',
    'description' => 'Import blog articles from CSV file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'blog_article',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'outlet brand',
            'target' => 'domains:multiple',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'title',
            'target' => 'title',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'published',
            'target' => 'created',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'body',
            'target' => 'body',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'meta description',
            'target' => 'meta_description',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'tags',
            'target' => 'field_article_tags',
            'term_search' => '0',
            'autocreate' => 1,
          ),
          7 => array(
            'source' => 'new path',
            'target' => 'path_alias',
            'pathauto_override' => 1,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => '13',
        'authorize' => 0,
        'update_non_existent' => 'skip',
        'skip_hash_check' => 0,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['blog_import'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'owners';
  $feeds_importer->config = array(
    'name' => 'Owners import',
    'description' => 'Import store owners from CSV file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsUserProcessor',
      'config' => array(
        'bundle' => 'user',
        'roles' => array(
          5 => '5',
          3 => 0,
          6 => 0,
          4 => 0,
        ),
        'update_existing' => '2',
        'status' => '0',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => FALSE,
          ),
          1 => array(
            'source' => 'Email Address',
            'target' => 'mail',
            'unique' => 1,
          ),
          2 => array(
            'source' => 'Email Address',
            'target' => 'name',
            'unique' => 1,
          ),
          3 => array(
            'source' => 'outlet brand',
            'target' => 'domain_user:multiple',
            'unique' => FALSE,
          ),
        ),
        'defuse_mail' => 0,
        'update_non_existent' => 'skip',
        'input_format' => 'plain_text',
        'skip_hash_check' => 1,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['owners'] = $feeds_importer;

  $feeds_importer = new stdClass();
  $feeds_importer->disabled = FALSE; /* Edit this to true to make a default feeds_importer disabled initially */
  $feeds_importer->api_version = 1;
  $feeds_importer->id = 'store_import';
  $feeds_importer->config = array(
    'name' => 'Stores import',
    'description' => 'Import stores from CSV file.',
    'fetcher' => array(
      'plugin_key' => 'FeedsFileFetcher',
      'config' => array(
        'direct' => FALSE,
        'allowed_extensions' => 'txt csv tsv xml opml',
        'directory' => 'private://feeds',
        'allowed_schemes' => array(
          0 => 'public',
          1 => 'private',
        ),
      ),
    ),
    'parser' => array(
      'plugin_key' => 'FeedsCSVParser',
      'config' => array(
        'delimiter' => ',',
        'no_headers' => 0,
      ),
    ),
    'processor' => array(
      'plugin_key' => 'FeedsNodeProcessor',
      'config' => array(
        'bundle' => 'store',
        'update_existing' => '2',
        'expire' => '-1',
        'mappings' => array(
          0 => array(
            'source' => 'guid',
            'target' => 'guid',
            'unique' => 1,
          ),
          1 => array(
            'source' => 'store name',
            'target' => 'title',
            'unique' => FALSE,
          ),
          2 => array(
            'source' => 'short name',
            'target' => 'field_store_shortname',
            'unique' => FALSE,
          ),
          3 => array(
            'source' => 'outlet brand',
            'target' => 'domains:multiple',
            'unique' => FALSE,
          ),
          4 => array(
            'source' => 'address line 1',
            'target' => 'field_store_address:thoroughfare',
            'unique' => FALSE,
          ),
          5 => array(
            'source' => 'address line 2',
            'target' => 'field_store_address:premise',
            'unique' => FALSE,
          ),
          6 => array(
            'source' => 'address suburb',
            'target' => 'field_store_address:locality',
            'unique' => FALSE,
          ),
          7 => array(
            'source' => 'address state',
            'target' => 'field_store_address:administrative_area',
            'unique' => FALSE,
          ),
          8 => array(
            'source' => 'address postcode',
            'target' => 'field_store_address:postal_code',
            'unique' => FALSE,
          ),
          9 => array(
            'source' => 'address country',
            'target' => 'field_store_address:country',
            'unique' => FALSE,
          ),
          10 => array(
            'source' => 'telephone',
            'target' => 'field_store_telephone',
            'unique' => FALSE,
          ),
          11 => array(
            'source' => 'facebook',
            'target' => 'field_facebook:url',
            'unique' => FALSE,
          ),
          12 => array(
            'source' => 'who we are',
            'target' => 'body',
            'format' => 'basic_wysiwyg',
          ),
          13 => array(
            'source' => 'store features',
            'target' => 'field_store_features',
            'term_search' => '0',
            'autocreate' => 0,
          ),
          14 => array(
            'source' => 'trading hours monday',
            'target' => 'field_store_trading_hours:hours:mon',
            'unique' => FALSE,
          ),
          15 => array(
            'source' => 'trading hours tuesday',
            'target' => 'field_store_trading_hours:hours:tues',
            'unique' => FALSE,
          ),
          16 => array(
            'source' => 'trading hours wednesday',
            'target' => 'field_store_trading_hours:hours:wednes',
            'unique' => FALSE,
          ),
          17 => array(
            'source' => 'trading hours thursday',
            'target' => 'field_store_trading_hours:hours:thurs',
            'unique' => FALSE,
          ),
          18 => array(
            'source' => 'trading hours friday',
            'target' => 'field_store_trading_hours:hours:fri',
            'unique' => FALSE,
          ),
          19 => array(
            'source' => 'trading hours saturday',
            'target' => 'field_store_trading_hours:hours:satur',
            'unique' => FALSE,
          ),
          20 => array(
            'source' => 'trading hours sunday',
            'target' => 'field_store_trading_hours:hours:sun',
            'unique' => FALSE,
          ),
          21 => array(
            'source' => 'status',
            'target' => 'status',
            'unique' => FALSE,
          ),
        ),
        'input_format' => 'plain_text',
        'author' => 0,
        'authorize' => 0,
        'update_non_existent' => 'skip',
        'skip_hash_check' => 1,
      ),
    ),
    'content_type' => '',
    'update' => 0,
    'import_period' => '-1',
    'expire_period' => 3600,
    'import_on_create' => 1,
    'process_in_background' => 0,
  );
  $export['store_import'] = $feeds_importer;

  return $export;
}
