<?php
/**
 * @file
 * feat_contexts.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function feat_contexts_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'sitewide' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'alm_store_locator-store_locator_nearest' => array(
          'module' => 'alm_store_locator',
          'delta' => 'store_locator_nearest',
          'region' => 'header',
          'weight' => '-10',
        ),
        'search-form' => array(
          'module' => 'search',
          'delta' => 'form',
          'region' => 'header',
          'weight' => '-8',
        ),
        'menu_block-2' => array(
          'module' => 'menu_block',
          'delta' => '2',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'menu_block-3' => array(
          'module' => 'menu_block',
          'delta' => '3',
          'region' => 'footer',
          'weight' => '-9',
        ),
        'menu_block-4' => array(
          'module' => 'menu_block',
          'delta' => '4',
          'region' => 'footer',
          'weight' => '-8',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;
  $export['sitewide'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide-cellarbrations';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        1 => 1,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-read-our-blog' => array(
          'module' => 'bean',
          'delta' => 'read-our-blog',
          'region' => 'header',
          'weight' => '-5',
        ),
        'bean-cellarbrations-facebook' => array(
          'module' => 'bean',
          'delta' => 'cellarbrations-facebook',
          'region' => 'header',
          'weight' => '0',
        ),
        'bean-cellarbrations-social-media' => array(
          'module' => 'bean',
          'delta' => 'cellarbrations-social-media',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'bean-cellarbrations-copyright' => array(
          'module' => 'bean',
          'delta' => 'cellarbrations-copyright',
          'region' => 'footer',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['sitewide-cellarbrations'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide-duncans';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        4 => 4,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-duncans-facebook' => array(
          'module' => 'bean',
          'delta' => 'duncans-facebook',
          'region' => 'header',
          'weight' => '5',
        ),
        'bean-duncans-social-media' => array(
          'module' => 'bean',
          'delta' => 'duncans-social-media',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'bean-duncans-copyright' => array(
          'module' => 'bean',
          'delta' => 'duncans-copyright',
          'region' => 'footer',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['sitewide-duncans'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide-iga-liquor';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        3 => 3,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-read-our-blog' => array(
          'module' => 'bean',
          'delta' => 'read-our-blog',
          'region' => 'header',
          'weight' => '-5',
        ),
        'bean-iga-facebook' => array(
          'module' => 'bean',
          'delta' => 'iga-facebook',
          'region' => 'header',
          'weight' => '0',
        ),
        'bean-iga-social-media' => array(
          'module' => 'bean',
          'delta' => 'iga-social-media',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'bean-iga-copyright' => array(
          'module' => 'bean',
          'delta' => 'iga-copyright',
          'region' => 'footer',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['sitewide-iga-liquor'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'sitewide-thebottle-o-au';
  $context->description = '';
  $context->tag = '';
  $context->conditions = array(
    'domain' => array(
      'values' => array(
        5 => 5,
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'bean-read-our-blog' => array(
          'module' => 'bean',
          'delta' => 'read-our-blog',
          'region' => 'header',
          'weight' => '-5',
        ),
        'bean-the-bottle-o-au-facebook' => array(
          'module' => 'bean',
          'delta' => 'the-bottle-o-au-facebook',
          'region' => 'header',
          'weight' => '0',
        ),
        'bean-the-bottle-o-au-social-media' => array(
          'module' => 'bean',
          'delta' => 'the-bottle-o-au-social-media',
          'region' => 'footer',
          'weight' => '-10',
        ),
        'bean-the-bottle-o-copyright' => array(
          'module' => 'bean',
          'delta' => 'the-bottle-o-copyright',
          'region' => 'footer',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 1;
  $export['sitewide-thebottle-o-au'] = $context;

  return $export;
}
