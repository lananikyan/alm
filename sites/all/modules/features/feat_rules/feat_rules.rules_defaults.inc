<?php
/**
 * @file
 * feat_rules.rules_defaults.inc
 */

/**
 * Implements hook_default_rules_configuration().
 */
function feat_rules_default_rules_configuration() {
  $items = array();
  $items['rules_content_back_to_draft'] = entity_import('rules_config', '{ "rules_content_back_to_draft" : {
      "LABEL" : "Content back to draft",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "workbench_extra", "workbench_moderation", "rules" ],
      "ON" : { "workbench_moderation_after_moderation_transition" : [] },
      "IF" : [
        { "contents_state_changed" : { "node" : [ "node" ] } },
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "draft" } }
      ],
      "DO" : [
        { "mail" : {
            "to" : "[node:author:mail]",
            "subject" : "[site:name] Your content has not been published: [node:title]",
            "message" : "Hello,\\r\\n\\r\\nYour change in the [node:content-type] [node:title] has not been approved. Here is the moderator comment:\\r\\nLast revision note:\\r\\n[node:log]\\r\\n\\r\\nLast moderation note:\\r\\n[node:workbench-moderation-notes]\\r\\n\\r\\n\\r\\nPlease log in to the website and review this content: [node:url]\\r\\n\\r\\nSincerely,\\r\\n",
            "language" : [ "" ]
          }
        }
      ]
    }
  }');
  $items['rules_content_needs_review'] = entity_import('rules_config', '{ "rules_content_needs_review" : {
      "LABEL" : "Content needs review",
      "PLUGIN" : "reaction rule",
      "OWNER" : "rules",
      "REQUIRES" : [ "workbench_extra", "workbench_moderation", "rules" ],
      "ON" : { "workbench_moderation_after_moderation_transition" : [] },
      "IF" : [
        { "contents_state_changed" : { "node" : [ "node" ] } },
        { "contents_current_state" : { "node" : [ "node" ], "moderation_state" : "needs_review" } }
      ],
      "DO" : [
        { "mail_to_users_of_role" : {
            "roles" : { "value" : { "4" : "4" } },
            "subject" : "[site:name] Content needs review: [node:title]",
            "message" : "Hello,\\r\\n\\r\\nA [node:content-type] has been updated by [site:current-user]. Here is the related moderation note:\\r\\nLast revision note:\\r\\n[node:log]\\r\\n\\r\\nLast moderation note:\\r\\n[node:workbench-moderation-notes]\\r\\n\\r\\nPlease log in to the website and review this content: \\r\\n[node:url]\\r\\n\\r\\nSincerely,"
          }
        }
      ]
    }
  }');
  return $items;
}
