var gulp = require("gulp"); //https://www.npmjs.com/package/gulp/
var fs = require("fs");
var $ = require("gulp-load-plugins")(); //https://www.npmjs.com/package/gulp-load-plugins

var THEMES = "sites/all/themes";
var subThemes = [
    "bottleo",
    "cellarbrations",
    "iga_liquor",
    "duncans"
];

// compile less to css
// https://www.npmjs.com/package/gulp-less/
gulp.task("less", function () {
    var retval;

    subThemes.forEach(function (theme) {
        retval = gulp.src([THEMES + "/" + theme + "/css/custom-layout.less"])
            .pipe($.sourcemaps.init())
            .pipe($.less())
            .pipe($.sourcemaps.write())
            .pipe(gulp.dest(THEMES + "/" + theme + "/css"));
    });

    return retval;
});


// watch for file changes and trigger tasks
// https://github.com/gulpjs/gulp/blob/master/docs/API.md#gulpwatchglob--opts-tasks-or-gulpwatchglob--opts-cb
gulp.task('watch', function () {
    gulp.watch([THEMES + '/**/*.less'], ['less']);
});


// trigger build tasks
// https://github.com/gulpjs/gulp/blob/master/docs/API.md#gulptaskname--deps-fn
gulp.task('build', ['less']);


gulp.task('default', ['build', 'watch'], function () {
    // place code for your default task here
});
